provider "aws" {
  region = "us-east-1"
}

resource "aws_security_group" "allow_ssh_http" {
  name        = "allow_ssh_http"
  description = "Allow SSH and HTTP traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "tls_private_key" "deployer_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = tls_private_key.deployer_key.public_key_openssh
}

resource "aws_instance" "web_server_instance" {
  ami           = "ami-04581fbf744a7d11f" # Amazon Linux 2 LTS AMI
  instance_type = "t2.micro"

  vpc_security_group_ids = [aws_security_group.allow_ssh_http.id]

  key_name = aws_key_pair.deployer.key_name

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = tls_private_key.deployer_key.private_key_pem
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum -y update",
      "sudo yum -y install httpd",
      "sudo systemctl start httpd",
      "sudo systemctl enable httpd",
    ]
  }

  provisioner "local-exec" {
    command = "echo The public IP is ${self.public_ip} > ip_address.txt"
  }

  provisioner "file" {
    source      = "index.html"
    destination = "/tmp/index.html"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/index.html /var/www/html/index.html",
    ]
  }

  tags = {
    Name = "web_server_instance"
  }

}

resource "null_resource" "example" {
  provisioner "local-exec" {
    command = "curl ${aws_instance.web_server_instance.public_ip} && echo The EC2 instance with IP http://${aws_instance.web_server_instance.public_ip} is up and running."
  }
}
