provider "aws" {
  region = "us-east-1"
}

module "ec2_instance" {
  source      = "./modules/ec2"
}

output "instance_id" {
  value = module.ec2_instance.instance_id
}

output "public_ip" {
  value = module.ec2_instance.public_ip
}
