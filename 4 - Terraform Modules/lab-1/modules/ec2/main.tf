data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_security_group" "ec2-sg" {
  count = var.security_group_id == "" ? 1 : 0

  name        = "ec2-instance-sg"
  description = "Security group for EC2 instance"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "demo-instance" {
  ami           = var.ami_id != "" ? var.ami_id : data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  vpc_security_group_ids = [
    coalesce(var.security_group_id, aws_security_group.ec2-sg[0].id)
  ]

  tags = {
    Name = "My EC2 Instance"
  }
}

output "instance_id" {
  description = "The ID of the created EC2 instance"
  value       = aws_instance.demo-instance.id
}

output "public_ip" {
  description = "The public IP address of the created EC2 instance"
  value       = aws_instance.demo-instance.public_ip
}
