variable "ami_id" {
  description = "The Amazon Machine Image (AMI) ID for the EC2 instance"
  type        = string
  default     = ""
}

variable "instance_type" {
  description = "The EC2 instance type"
  type        = string
  default     = "t2.micro"
}

variable "security_group_id" {
  description = "The security group ID for the EC2 instance"
  type        = string
  default     = ""
}
