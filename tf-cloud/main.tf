provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "web_server_instance" {
  ami           = "ami-04581fbf744a7d11f" # Amazon Linux 2 LTS AMI
  instance_type = "t2.micro"

  tags = {
    Name = "terraform-test"
  }
}
