### first_ec2.tf

```sh
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.0"
    }
    github = {
      source = "integrations/github"
      version = "5.18.3"
    }
  }
}

provider "aws" {
  region     = "us-east-1"
  access_key = "PUT-YOUR-ACCESS-KEY-HERE"
  secret_key = "PUT-YOUR-SECRET-KEY-HERE"
}
provider "github" {
  token = $GITHUB_TOKEN 
}

resource "aws_instance" "my_ec2_instance" {
  ami           = "ami-02f3f602d23f1659d"
  instance_type = "t3.micro"

  tags = {
    Name = "MyFirstInstance"
  }
}

resource "github_repository" "example_tf_repo" {
  name         = "example"
  description  = "My awesome codebase"
  has_projects = true
}
```
### Commands:

```sh
terraform init
terraform plan
terraform apply
```

### Version Constraint Examples:
- ">=3.0.0"
- "=4.7.0"
- "!=4.59.0"
- "<=3.0.0"
- "~>3.0"
- ">=4.7.0, <=4.9.0"