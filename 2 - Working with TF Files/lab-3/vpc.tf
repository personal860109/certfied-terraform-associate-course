resource "aws_vpc" "main" {
  cidr_block = var.base_cidr_block
  tags = {
    Name = "main-vpc"
  }
}

resource "aws_subnet" "public" {
  for_each = local.public_subnets

  cidr_block = each.value
  vpc_id     = aws_vpc.main.id

  tags = {
    Name   = "public-${each.key}"
    Type   = "public"
    Parent = "main-vpc"
  }
}

resource "aws_subnet" "private" {
  for_each = local.private_subnets

  cidr_block = each.value
  vpc_id     = aws_vpc.main.id

  tags = {
    Name   = "private-${each.key}"
    Type   = "private"
    Parent = "main-vpc"
  }
}

resource "aws_security_group" "web" {
  name        = "web-sg"
  description = "Web Security Group"
  vpc_id      = aws_vpc.main.id

  dynamic "ingress" {
    for_each = local.sg_rules.web.ingress

    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  dynamic "egress" {
    for_each = local.sg_rules.web.egress

    content {
      from_port   = egress.value.from_port
      to_port     = egress.value.to_port
      protocol    = egress.value.protocol
      cidr_blocks = egress.value.cidr_blocks
    }
  }
}

resource "aws_security_group" "db" {
  name        = "db-sg"
  description = "DB Security Group"
  vpc_id      = aws_vpc.main.id

  dynamic "ingress" {
    for_each = local.sg_rules.db.ingress

    content {
      from_port       = ingress.value.from_port
      to_port         = ingress.value.to_port
      protocol        = ingress.value.protocol
      security_groups = [aws_security_group.web.id]
    }
  }

  dynamic "egress" {
    for_each = local.sg_rules.db.egress

    content {
      from_port       = egress.value.from_port
      to_port         = egress.value.to_port
      protocol        = egress.value.protocol
      cidr_blocks     = egress.value.cidr_blocks
      security_groups = [aws_security_group.web.id]
    }
  }
}

