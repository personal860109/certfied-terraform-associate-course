variable "base_cidr_block" {
  description = "The base CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "instances" {
  description = "A map of instances with their names and other information"
}

variable "instance_type" {
  description = "The instance type to use for the instances"
  type        = string
  default     = "t2.micro"
}