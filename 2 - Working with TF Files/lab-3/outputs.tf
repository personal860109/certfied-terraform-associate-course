output "instance_information" {
  value = {
    for name in keys(var.instances) : name => {
      id                 = aws_instance.instance[name].id
      subnet_id          = aws_instance.instance[name].subnet_id
      publicly_accesible = contains(keys(local.public_subnets), name)
    }
  }
}

