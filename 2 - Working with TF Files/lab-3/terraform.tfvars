instances = {
  web1 = {
    is_public     = true
    ami_id        = ""
    instance_type = "t2.micro"
  }
  web2 = {
    is_public     = true
    ami_id        = ""
    instance_type = "t2.micro"
  }
  web3 = {
    is_public     = true
    ami_id        = ""
    instance_type = "t2.micro"
  }
  web4 = {
    is_public     = true
    ami_id        = ""
    instance_type = "t2.micro"
  }
  web5 = {
    is_public     = true
    ami_id        = ""
    instance_type = "t2.micro"
  }
  db1 = {
    is_public     = false
    ami_id        = ""
    instance_type = "t2.medium"
  }
  db2 = {
    is_public     = false
    ami_id        = ""
    instance_type = "t2.medium"
  }
}