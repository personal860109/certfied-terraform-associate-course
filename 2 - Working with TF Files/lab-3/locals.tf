locals {
  public_subnets = {
    for idx, key in zipmap(range(length(var.instances)), keys(var.instances)) : key => cidrsubnet(var.base_cidr_block, 8, 2 + idx) if var.instances[key].is_public == true
  }
  private_subnets = {
    for idx, key in zipmap(range(length(var.instances)), keys(var.instances)) : key => cidrsubnet(var.base_cidr_block, 8, 20 + idx) if var.instances[key].is_public == false
  }
}

locals {
  sg_rules = {
    web = {
      ingress = [
        {
          name        = "ssh"
          from_port   = 22
          to_port     = 22
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        },
        {
          name        = "http"
          from_port   = 80
          to_port     = 80
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
      ]
      egress = [
        {
          name        = "all"
          from_port   = 22
          to_port     = 22
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
      ]
    }
    db = {
      ingress = [
        {
          name      = "mysql"
          from_port = 3306
          to_port   = 3306
          protocol  = "tcp"
        },
        {
          name      = "ssh"
          from_port = 22
          to_port   = 22
          protocol  = "tcp"
        }
      ]
      egress = []
    }
  }
}