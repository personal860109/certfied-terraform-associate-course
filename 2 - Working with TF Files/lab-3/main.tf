provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "instance" {
  for_each = var.instances

  ami           = each.value.ami_id == "" ? data.aws_ami.ami_information.id : each.value.ami_id
  instance_type = each.value.instance_type == "" ? var.instance_type : each.value.instance_type

  subnet_id = each.value.is_public == true ? aws_subnet.public[each.key].id : aws_subnet.private[each.key].id

  vpc_security_group_ids = each.key == "db" ? [aws_security_group.db.id] : [aws_security_group.web.id]

  tags = {
    Name = each.key
  }
}
