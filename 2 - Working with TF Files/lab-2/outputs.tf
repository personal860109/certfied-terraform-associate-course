output "vpc_id" {
  value = data.aws_vpc.default.id
}

output "instance_public_ips" {
  value = aws_instance.web.*.public_ip
}
