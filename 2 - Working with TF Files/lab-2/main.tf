resource "aws_security_group" "instance" {
  name_prefix = "instance-sg"
  vpc_id      = data.aws_vpc.default.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.allowed_ip_cidr]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.allowed_ip_cidr]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.allowed_ip_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  count = length(data.aws_subnets.default_subnets.ids)

  ami           = data.aws_ami.ami_information.id
  instance_type = var.instance_type

  vpc_security_group_ids = [aws_security_group.instance.id]
  subnet_id              = data.aws_subnets.default_subnets.ids[count.index]

  tags = {
    Name = "web-${count.index + 1}"
  }
}
