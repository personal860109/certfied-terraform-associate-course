locals {
  ami_id = var.is_prod == true ? [local.ami_wildcards.ubuntu] : [local.ami_wildcards.amazon-linux-2]
}

locals {
  ami_wildcards = {
    ubuntu         = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*",
    amazon-linux-2 = "amzn2-ami-hvm-*-x86_64-gp2"
  }
}
