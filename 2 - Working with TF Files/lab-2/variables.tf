variable "aws_region" {
  description = "The AWS region where resources will be created"
  default     = "us-east-1"
}

variable "allowed_ip_cidr" {
  description = "The IP CIDR allowed to access the instances"
  type        = string
  default     = "0.0.0.0/0"
}

variable "instance_type" {
  description = "Type of EC2 instance to be provisioned"
  type        = string
  default     = "t2.micro"
}

variable "is_prod" {
  description = "Is this going to be a PROD instance? Please type 'true' or 'false'"
  type        = bool
}
