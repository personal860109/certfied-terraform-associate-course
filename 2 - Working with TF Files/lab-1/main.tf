provider "aws" {
  region = "us-east-1"
}

resource "aws_security_group" "ec2_security_group" {
  name_prefix = "ec2_security_group_"
  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = [var.ip_cidr]
  }
  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = [var.ip_cidr]
  }
  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = [var.ip_cidr]
  }
  tags = {
    Name = "ec2_security_group"
  }
}

resource "aws_instance" "example" {
  ami           = var.ami_id
  instance_type = var.instance_type

  vpc_security_group_ids = [
    aws_security_group.ec2_security_group.id,
  ]
}
