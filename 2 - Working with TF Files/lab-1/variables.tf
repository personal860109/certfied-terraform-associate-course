variable "ami_id" {
  description = "The ID of the AMI to use for the EC2 instance"
  default     = "ami-0c55b159cbfafe1f0" # Ubuntu 20.04 LTS
}

variable "instance_type" {
  description = "The type of EC2 instance to launch"
  default     = "t2.micro"
}

variable "ip_cidr" {
  description = "The IP address range to allow traffic from"
  default     = "0.0.0.0/0"
}